const std = @import("std");
const mem = std.mem;
const Allocator = mem.Allocator;
const TypeId = @import("builtin").TypeId;
const meta = @import("meta.zig");

// Growable appender
//  Appender is based on a buffer of type T
//  Appender must call deinit to free used memory
//  The buffer uses a supplied *Allocator to alloc, realloc, and free the buffer
//  The buffer has a capactity, and used length, can be reset and resized
//
//  Appender is *NOT* thread safe  TODO: make a thread safe Appender (wrapper?)
//  Appender currently only grows to required length + 64 bytes (TODO: add grow strategy)
pub fn Appender(comptime T: type) type {
    return struct {
        const Self = this;

        // Never access these directly
        buf: []T, // internal buffer
        used: usize, // used length of the buffer
        alloc: *Allocator, // allocator holding the buffer memeory
        dead: bool, // flag, used to check that the Appender is still valid

        // Creates the Appender on the given Allocator, reserves the inital size with a mini-size of 64
        pub fn init(alloc: *Allocator, size: usize) !Self {
            // Only allow basic types
            comptime if (meta.isPointer(T)) @compileError("Appender cannot be of Pointer type");
            comptime if (meta.isArray(T)) @compileError("Appender cannot be of Array type");
            comptime if (meta.isUnion(T)) @compileError("Appender cannot be of Union type");
            comptime if (meta.isEnum(T)) @compileError("Appender cannot be of Enum type");
            comptime if (meta.isFunction(T)) @compileError("Appender cannot be of Function type");

            return Self{
                .buf = try alloc.alloc(T, if (size == 0) 64 else size),
                .used = 0,
                .alloc = alloc,
                .dead = false,
            };
        }

        // Free's used memory from the Allocator, safe to call multiple times
        pub fn deinit(self: *Self) void {
            defer self.dead = true;
            if (!self.dead) self.alloc.free(self.buf);
        }

        // Returns the used length of the Appender
        pub fn len(self: *const Self) usize {
            std.debug.assert(!self.dead);
            return self.used;
        }

        // Returns the current capacity of the Appender, this is how many bytes that can be added without having to call realloc
        pub fn capacity(self: *const Self) usize {
            std.debug.assert(!self.dead);
            return self.buf.len;
        }

        // Sets the buffer size to a given size, only calls realloc if the size needs to change, fails if the resize would truncate the used buffer
        pub fn reserve(self: *Self, new_size: usize) !void {
            std.debug.assert(!self.dead);
            if (new_size < self.used) return error.ReserveWouldTruncateBuffer;
            if (new_size != self.len()) self.buf = try self.alloc.realloc(T, self.buf, new_size);
        }

        // Helper to makes sure the buffer is large enough to hold the expected buffer size
        fn ensure(self: *Self, new_size: usize) !void {
            std.debug.assert(!self.dead);
            if (new_size > self.buf.len) {
                self.buf = try self.alloc.realloc(T, self.buf, new_size + 64);
            }
        }

        // Resets the Appender, used length is now 0
        pub fn reset(self: *Self) void {
            std.debug.assert(!self.dead);
            self.used = 0;
        }

        // Returns a slice to the Appender data
        pub fn data(self: *Self) []T {
            std.debug.assert(!self.dead);
            return self.buf[0..self.used];
        }

        pub fn put(self: *Self, args: ...) !void {
            std.debug.assert(!self.dead);

            // compute size of args
            comptime var args_index: usize = 0;
            var args_size: usize = 0;
            inline while (args_index < args.len) : (args_index += 1) {
                const arg = args[args_index];
                comptime const arg_t = comptime if (meta.isPointer(@typeOf(arg))) meta.pointerOf(@typeOf(arg)) else @typeOf(arg);
                comptime if (meta.isArray(arg_t) and meta.arrayOf(arg_t) == T) {
                    args_size += arg.len;
                } else if (arg_t == T) {
                    args_size += 1;
                } else {
                    @compileError("Appender can only insert types of T");
                };
            }

            // make sure the buf is large enough
            try self.ensure(self.used + args_size);

            args_index = 0;
            inline while (args_index < args.len) : (args_index += 1) {
                const arg = args[args_index];
                const arg_t = comptime if (meta.isPointer(@typeOf(arg))) meta.pointerOf(@typeOf(arg)) else @typeOf(arg);
                if (arg_t == T) {
                    self.buf[self.used] = arg;
                    self.used += 1;
                } else if (meta.isArray(arg_t) and meta.arrayOf(arg_t) == T) {
                    mem.copy(T, self.buf[self.used..], arg);
                    self.used += arg.len;
                }
            }
        } // put
    }; // struct
}

// Creates an Appender(T) using a given allocator
pub fn appender(comptime T: type, alloc: *Allocator) !Appender(T) {
    return Appender(T).init(alloc, 0);
}

test "u8 Appender" {
    const assert = std.debug.assert;

    var a = try appender(u8, std.debug.global_allocator);
    defer a.deinit();
    assert(a.len() == 0);
    //try a.put("123", u8('h'), u8('i'));
    try a.put(u8('h'), u8('e'));
    try a.put("llo");
    assert(a.len() == 5);
    assert(mem.eql(u8, a.data(), "hello"));
    a.reset();
    assert(a.len() == 0);
    assert(a.capacity() >= 64);
}

test "i32 Appender" {
    const assert = std.debug.assert;

    var a = try appender(i32, std.debug.global_allocator);
    defer a.deinit();
    try a.reserve(6);
    assert(a.capacity() == 6);

    try a.put(i32(1), i32(2), i32(3), i32(4), i32(5), i32(6));
    assert(a.capacity() == 6);
    assert(a.len() == 6);
    assert(a.data()[3] == 4);
    assert(mem.eql(i32, a.data(), []i32{ 1, 2, 3, 4, 5, 6 }));
}

test "realloc Testing" {
    const assert = std.debug.assert;

    var a = try appender(u8, std.debug.global_allocator);
    defer a.deinit();

    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    assert(a.len() == 90);
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    try a.put("123456789012345678901234567890");
    assert(a.len() == 390);
    assert(a.capacity() >= 390);
}

