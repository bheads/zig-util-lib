const std = @import("std");
const mem = std.mem;
const builtin = @import("builtin");
const TypeId = builtin.TypeId;
const TypeInfo = builtin.TypeInfo;

const assert = std.debug.assert;

////////////////////////// Type utils ///////////////////////

pub inline fn isTypeId(comptime T: var, comptime Tid: TypeId) bool {
    comptime return @typeId(asType(T)) == Tid;
}

pub inline fn asType(comptime T: var) type { // T may not be a type, get the type or return T
    comptime return switch (@typeId(@typeOf(T))) {
        TypeId.Type => T,
        else => @typeOf(T),
    };
}

pub inline fn isType(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Type);
}

pub inline fn isVoid(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Void);
}

pub inline fn isNoReturn(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.NoReturn);
}

pub inline fn isUndefined(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Undefined);
}

pub inline fn isNull(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Null);
}

pub inline fn isOptional(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Optional);
}

pub inline fn isErrorUnion(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.ErrorUnion);
}

// TODO: test these

////////////////////////// Funtion Utils ////////////////////

pub inline fn isFunction(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Fn);
}

pub inline fn isFunctionPtr(comptime T: var) bool {
    comptime if (!isPointer(T)) return false;
    comptime return isFunction(pointerOf(T));
}

pub inline fn returnOf(comptime T: var) type {
    comptime if (!isFunction(T) and !isFunctionPtr(T)) @compileError("returnOf must be of Function type");
    comptime const FT = asType(if (isFunctionPtr(T)) pointerOf(T) else T); // unwrap the function pointer
    comptime return @typeInfo(FT).Fn.return_type;
}

test "isFunction" {
    assert(isFunction(@typeOf(isFunction)));
    assert(isFunction(isFunction));
    assert(!isFunction(&isFunction)); // function pointer not allowed

    var x: i32 = 0;
    assert(!isFunction(@typeOf(x))); //have to use typeOf since this is not const or comptime
    assert(!isFunction(@typeOf(@typeOf(x))));
    assert(!isFunction(@typeOf(&x)));

    const y: f32 = 0;
    assert(!isFunction(y));
    assert(!isFunction(&y));

    const A = struct {};
    assert(!isFunction(@typeOf(A)));
    assert(!isFunction(A));

    const B = union {
        x: i32,
    };
    assert(!isFunction(@typeOf(B)));
    assert(!isFunction(B));

    const C = enum {
        OK,
        NOTOK,
    };
    assert(!isFunction(@typeOf(C)));
    assert(!isFunction(C));

    assert(!isFunction(std));
}

test "isFunctionPtr" {
    assert(!isFunctionPtr(@typeOf(isFunction)));
    assert(!isFunctionPtr(isFunction));
    assert(isFunctionPtr(&isFunction));
    assert(isFunctionPtr(@typeOf(&isFunction)));
    assert(isFunctionPtr(@typeOf(&isFunctionPtr)));

    var x: i32 = 0;
    assert(!isFunctionPtr(@typeOf(x))); //have to use typeOf since this is not const or comptime
    assert(!isFunctionPtr(@typeOf(@typeOf(x))));
    assert(!isFunctionPtr(@typeOf(&x)));

    const y: f32 = 0;
    assert(!isFunctionPtr(y));
    assert(!isFunctionPtr(&y));

    const A = struct {};
    assert(!isFunctionPtr(@typeOf(A)));
    assert(!isFunctionPtr(A));

    const B = union {
        x: i32,
    };
    assert(!isFunctionPtr(@typeOf(B)));
    assert(!isFunctionPtr(B));

    const C = enum {
        OK,
        NOTOK,
    };
    assert(!isFunctionPtr(@typeOf(C)));
    assert(!isFunctionPtr(C));

    assert(!isFunctionPtr(std));
}

test "returnOf" {
    // comptime functions are undefined
    assert(returnOf(returnOf) == @typeOf(undefined));
    assert(isUndefined(returnOf(returnOf)));

    const A = fn () i32;
    assert(returnOf(A) == i32);

    assert(returnOf(fn () void) == void);
    assert(isVoid(returnOf(fn () void)));
    assert(isNoReturn(returnOf(fn () noreturn)));
    assert(isOptional(returnOf(fn () ?i32)));
    // assert(isError(returnOf(fn() error)));// is TypeId.Error valid anymore
    assert(returnOf(fn () error!void) == error!void);
    assert(isErrorUnion(returnOf(fn () error!void)));
}

///////////////////// Pointer Utils /////////////////////

pub inline fn isPointer(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Pointer);
}

pub inline fn pointerOf(comptime T: var) type {
    comptime if (!isPointer(T)) @compileError("pointerOf T must be a pointer"); // must be a pointer
    comptime return @typeInfo(asType(T)).Pointer.child;
}

test "isPointer" {
    assert(!isPointer(@typeOf(isFunction)));
    assert(!isPointer(isFunction));
    assert(isPointer(&isFunction));

    var x: i32 = 0;
    assert(!isPointer(@typeOf(x))); //have to use typeOf since this is not const or comptime
    assert(!isPointer(@typeOf(@typeOf(x))));
    assert(isPointer(@typeOf(&x)));

    const y: f32 = 0;
    assert(!isPointer(y));
    assert(isPointer(&y));

    const A = struct {};
    assert(!isPointer(@typeOf(A)));
    assert(!isPointer(A));

    const B = union {
        x: i32,
    };
    assert(!isPointer(@typeOf(B)));
    assert(!isPointer(B));

    const C = enum {
        OK,
        NOTOK,
    };
    assert(!isPointer(@typeOf(C)));
    assert(!isPointer(C));

    assert(!isPointer(std));
}

test "pointerOf" { // TODO: more test cases
    const x: i32 = 0;
    assert(pointerOf(&x) == i32);
    assert(pointerOf(@typeOf(&x)) == i32);
    assert(pointerOf(&pointerOf) == @typeOf(pointerOf));
}

///////////////////// Struct Utils /////////////////////

pub inline fn isStruct(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Struct);
}

pub inline fn isStructPtr(comptime T: var) bool {
    comptime return isPointer(T) and isStruct(pointerOf(T));
}

// Is there a field with this name, TODO: can this also handle unions?
pub inline fn structHasField(comptime T: var, comptime name: []const u8, comptime FT: type) bool {
    comptime if (!isStruct(T) and !isStructPtr(T)) @compileError("structHasField T must be of Struct type"); // must be a struct
    comptime const ST = asType(comptime if (isStructPtr(T)) pointerOf(T) else T); // unwrap the struct pointer

    comptime const fields = @typeInfo(ST).Struct.fields;
    comptime for (fields) |field| {
        comptime if (FT == field.field_type and mem.eql(u8, name, field.name)) return true;
    };
    comptime return false;
}

// Is there a function with this name, TODO: can this also handle unions?
pub inline fn structHasFunction(comptime T: var, comptime name: []const u8, comptime FT: type) bool {
    comptime if (!isStruct(T) and !isStructPtr(T)) @compileError("structHasFunction T must be of Struct type");
    comptime if (!isFunction(FT)) @compileError("structHasFunction FT must be of Function type"); // T must be a struct and FT must be a function
    comptime const ST = asType(comptime if (isStructPtr(T)) pointerOf(T) else T); // unwrap the struct pointer

    comptime const defs = @typeInfo(ST).Struct.defs;
    comptime for (defs) |def| {
        comptime if (def.data == TypeInfo.Definition.Data.Fn and def.data.Fn.fn_type == FT and mem.eql(u8, name, def.name)) return true;
    };
    comptime return false;
}

test "isStruct" {
    assert(!isStruct(@typeOf(isFunction)));
    assert(!isStruct(isFunction));
    assert(!isStruct(&isFunction));
    assert(!isStruct(@typeOf(&isFunction)));
    assert(!isStruct(@typeOf(&isFunctionPtr)));

    var x: i32 = 0;
    assert(!isStruct(@typeOf(x))); //have to use typeOf since this is not const or comptime
    assert(!isStruct(@typeOf(@typeOf(x))));
    assert(!isStruct(@typeOf(&x)));

    const y: f32 = 0;
    assert(!isStruct(y));
    assert(!isStruct(&y));

    const A = struct {};
    assert(isStruct(A));
    var a = A{};
    assert(isStruct(@typeOf(a)));
    const a2 = A{};
    assert(isStruct(a2));
    assert(!isStruct(&a2));

    const B = union {
        x: i32,
    };
    assert(!isStruct(@typeOf(B)));
    assert(!isStruct(B));

    const C = enum {
        OK,
        NOTOK,
    };
    assert(!isStruct(@typeOf(C)));
    assert(!isStruct(C));

    assert(!isStruct(std)); // this maybe true in the future
}

test "isStructPtr" {
    assert(!isStructPtr(@typeOf(isFunction)));
    assert(!isStructPtr(isFunction));
    assert(!isStructPtr(&isFunction));
    assert(!isStructPtr(@typeOf(&isFunction)));
    assert(!isStructPtr(@typeOf(&isFunctionPtr)));

    var x: i32 = 0;
    assert(!isStructPtr(@typeOf(x))); //have to use typeOf since this is not const or comptime
    assert(!isStructPtr(@typeOf(@typeOf(x))));
    assert(!isStructPtr(@typeOf(&x)));

    const y: f32 = 0;
    assert(!isStructPtr(y));
    assert(!isStructPtr(&y));

    const A = struct {};
    assert(!isStructPtr(A));
    var a = A{};
    assert(!isStructPtr(@typeOf(a)));
    assert(isStructPtr(@typeOf(&a)));
    const a2 = A{};
    assert(!isStructPtr(a2));
    assert(isStructPtr(&a2));

    const B = union {
        x: i32,
    };
    assert(!isStructPtr(@typeOf(B)));
    assert(!isStructPtr(B));

    const C = enum {
        OK,
        NOTOK,
    };
    assert(!isStructPtr(@typeOf(C)));
    assert(!isStructPtr(C));

    assert(!isStructPtr(std));
}

test "structHasField" {
    assert(structHasField(struct {
        x: i32,
    }, "x", i32));
    assert(!structHasField(struct {
        x: i32,
    }, "y", i32));
    assert(!structHasField(struct {
        x: i32,
    }, "x", u32));
    assert(structHasField(struct {
        x: i32,
        y: ?f64,
        longer_name: fn () void,
    }, "longer_name", fn () void));
}

test "structHasFunction" {
    assert(structHasFunction(struct {
        fn foo() void {}
    }, "foo", fn () void));
    assert(structHasFunction(struct {
        fn foo(a: f64, b: ?i32) void {}
    }, "foo", fn (f64, ?i32) void));
    assert(!structHasFunction(struct {
        fn foo() void {}
    }, "foo2", fn () void));

    // BUG: uncommenting these last two lines causes the test to not report (not sure if its crashing)
    //std.debug.warn("{}\n", structHasFunction(mem.Allocator, "alloc", fn(*mem.Allocator, var, var) var));
    //assert(structHasFunction(mem.Allocator, "alloc", fn(*mem.Allocator, var, var) var));
}

///////////////////// Array Utils /////////////////////

pub inline fn isStaticArray(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Array);
}

pub inline fn isSlice(comptime T: var) bool {
    comptime return isPointer(T) and @typeInfo(asType(T)).Pointer.size == TypeInfo.Pointer.Size.Slice;
}

pub inline fn isArray(comptime T: var) bool {
    comptime return isStaticArray(T) or isSlice(T);
}

pub inline fn arrayOf(comptime T: var) type {
    comptime if (isStaticArray(T)) {
        return @typeInfo(asType(T)).Array.child;
    } else if (isSlice(T)) {
        return @typeInfo(asType(T)).Pointer.child;
    } else {
        @compileError("arrayOf T must be an array");
    };
}

test "isArray" { // testing still feels flaky
    assert(isArray(""));
    assert(isArray(asType("Hello")));
    assert(!isArray(5));
    assert(isArray("hello"[0..1]));
    assert(isSlice("hello"[0..1]));
    assert(!isSlice(asType([]u8{ 1, 2, 3, 4 })));
    //assert(isStaticArray(asType([]u8{ 1, 2, 3, 4 })));
    assert(isArray([]const i32));
}

test "arrayOf" {
    assert(arrayOf([]const u8) == u8);
    assert(arrayOf("hello"[0..]) == u8);
    assert(arrayOf([]const i32) == i32);
}

pub inline fn isUnion(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Union);
}

pub inline fn isEnum(comptime T: var) bool {
    comptime return isTypeId(T, TypeId.Enum);
}
